<?php

namespace App\Controller;

use App\Form\CardsType;
use App\Entity\Cards;
use App\Repository\CardsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;

class CardsController extends AbstractController
{    
    #[Route('/cards', name: 'app_cards')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/CardsController.php',
        ]);
    }

    #[Route('/cards/create', name: 'card_create')]
    public function new(Request $request, PersistenceManagerRegistry $doctrine): Response
    {
        $card = new Cards();
        $form = $this->createForm(CardsType::class, $card);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $card = $form->getData();

            $em = $doctrine->getManager();
            $em->persist($card);
            $em->flush();

            return $this->redirectToRoute('card_list');
        }

        return $this->render('cards/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/', name: 'card_list')]
    public function success(CardsRepository $cardsRepository): Response
    {
        $cards = $cardsRepository->findAll();

        return $this->render('cards/list.html.twig', [
            'cards' => $cards,
        ]);
    }

    #[Route('/cards/edit/{id}', name: 'card_edit')]
    public function edit(Cards $card, Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CardsType::class, $card);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('card_list');
        }

        return $this->render('cards/edit.html.twig', [
            'form' => $form->createView(),
            'card' => $card,
        ]);
    }

    #[Route('/cards/delete/{id}', name: 'card_delete')]
    public function delete(Cards $card, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($card);
        $entityManager->flush();

        return $this->redirectToRoute('card_list');
    }

    #[Route('/cards/details/{id}', name: 'card_details')]
    public function details(Cards $card): Response
    {
        return $this->render('cards/details.html.twig', [
            'card' => $card,
        ]);
    }
}
