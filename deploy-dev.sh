cp .env.dev ./app/.env
mkdir data/
mkdir data/dev
docker compose -f docker-compose.dev.yml up -d --build
docker exec -ti php.localhost wait-for-it -t 60 mysql.localhost:3306 -- php bin/console doctrine:migrations:migrate --no-interaction