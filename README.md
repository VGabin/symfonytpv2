Bonjour, j'ai fais deux commandes une pour pour exécuter la version dev et l'autre la version preprod :

Dev :
```
sh deploy-dev.sh
```

Preprod :
```
sh deploy-preprod.sh
```

Si jamais la migration ne ce fait pas à cause d'un timeout (qui est normalement géré) :

```
Dev : docker exec -ti php.localhost php bin/console doctrine:migrations:migrate --no-interaction
Preprod : docker exec -ti php.preprod php bin/console doctrine:migrations:migrate --no-interaction
```

docker run -it symfonytpv2-composer composer require api