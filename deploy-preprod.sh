cp .env.preprod ./app/.env
mkdir data/
mkdir data/preprod/
docker compose -f docker-compose.preprod.yml up -d --build
sleep 2
docker rm composer.preprod
docker exec -ti php.preprod wait-for-it -t 60  mysql.preprod:3306 -- php bin/console doctrine:migrations:migrate --no-interaction
